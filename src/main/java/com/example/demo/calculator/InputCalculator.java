package com.example.demo.calculator;

public class InputCalculator {
    private int curr;
    private int prev;

    public InputCalculator(int curr, int prev) {
        this.curr = curr;
        this.prev = prev;
    }

    public int add(){
        return this.prev + this.curr;
    }
    public int sub(){
        return this.prev - this.curr ;
    }
    public int mul(){
        return this.prev * this.curr;
    }
    public int div(){
        return this.prev / this.curr;
    }
}
