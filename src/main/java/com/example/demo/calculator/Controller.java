package com.example.demo.calculator;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class Controller {
    @GetMapping("/hello")
    public String helloWorld() {
        return "Hello World!";
    }

    @PostMapping("/add")
    public ResultCalculator add(@RequestBody InputCalculator input) {
        return new ResultCalculator(input.add());
    }
    @PostMapping("/subtract")
    public ResultCalculator subtract(@RequestBody InputCalculator input) {
        return new ResultCalculator(input.sub());
    }
    @PostMapping("/multiply")
    public ResultCalculator multiply(@RequestBody InputCalculator input) {
        return new ResultCalculator(input.mul());
    }
    @PostMapping("/divide")
    public ResultCalculator divide(@RequestBody InputCalculator input) {
        return new ResultCalculator(input.div());
    }
}


